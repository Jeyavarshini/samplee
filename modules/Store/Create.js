import {createStore, applyMiddleware} from 'redux';
import root from './root.js';
import thunk from 'redux-thunk'

export default function Create() {    
    const store = createStore(
        root,
        applyMiddleware(thunk)
        );
    return store;
    }