import React from 'react';
import DashboardComponent from '../Component/DashboardComponent.js';
//import NavbarContainer from '../../../Common/container/navbarContainer.js'
import HeaderContainer from '../../../Common/container/headerContainer'
import { getCookie } from '../../../assets/js/cookie.js';

class DashboardContainer extends React.Component {
    constructor(props) {
        super(props);
    }
render(){
    return (
        <div>
            <HeaderContainer/>
            <DashboardComponent/>
        </div>
            )
        }
    componentDidMount(){
        var token=getCookie("Authorization");
        if(token === undefined || token === null || token === "" )
        window.location="/"
    }
        
    }

export default DashboardContainer;
            