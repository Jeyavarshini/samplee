import * as types from '../Action/actiontypes';

const initialState = {
    loginError: '',
    Dashboard: ''
};

export default function LoginReducer(state = initialState, action) {
    switch (action.type) {

        case types.LOGIN_ERROR:
            return Object.assign({}, state, {
                loginError: (action.loginErrorMessage)
            });

        default:
            return state;
    }
};