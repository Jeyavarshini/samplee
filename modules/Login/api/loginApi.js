import {HTTP_AUTH_URL, HTTP_SERVER_URL} from '../../../Configuration/axios.configuration.js';
import {LOGIN_URL} from '../../../Configuration/ApiConfiguration.js';

class LoginApi{  
    
    static login(data) {
        return HTTP_AUTH_URL.post(LOGIN_URL, data).then(response => {
            return response;
        }).catch(error => {
            return error.response;
        });
    }
}
    export default LoginApi;