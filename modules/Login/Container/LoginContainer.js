import React,{PropTypes} from 'react';
import LoginForm from '../Component/LoginForm.js'
import {getCookie} from '../../../assets/js/cookie'
class LoginContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id:""
        }
    }
    
    render() {
        return (
            <div>
                <LoginForm />
            </div>
        );
    }

    componentDidMount() {
        var token = getCookie('Authorization');
        if(token != undefined && token != null && token != "")
         window.location = "/dashboard"
    }
}

export default LoginContainer;