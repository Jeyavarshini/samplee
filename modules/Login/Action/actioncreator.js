import * as types from '../Action/actiontypes';  

export function loginFinished(Dashboard) {
    return {type: types.LOGIN_FINISHED, Dashboard};
}

export function loginError(loginErrorMessage) {
    return {type: types.LOGIN_ERROR, loginErrorMessage};
}

