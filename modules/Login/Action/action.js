import * as actionCreator from './actioncreator.js'
import * as actionTypes from './actiontypes.js'
import {browserHistory} from 'react-router'
import LoginApi from '../api/loginApi.js'
import {setCookie,getCookie,clearCookie} from '../../../assets/js/cookie'

export function login(data) {
    return function(dispatch) {
        return LoginApi.login(data).then(response => {
            console.log(response)
            if(response.status == 200) {
                var token = response.data.token;
                setCookie("Authorization","Bearer" + " " + token);
                browserHistory.push("/dashboard")
            } else 
                dispatch(actionCreator.loginError("Login failed"));
        }).catch(error => {
            dispatch(actionCreator.loginFailed(error.response.data));
            return error.response
        });
    };
}
export function clearReduxState() {
    return function(dispatch) {
        dispatch(actioncreator.clearReduxState());     
    };
};
export function logout() {
    clearCookie("Authorization");
    //browserHistory.push("/")
    window.location = "/"
}