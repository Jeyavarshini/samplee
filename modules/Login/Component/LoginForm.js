import React from 'react';
import ReactDOM from 'react-dom';
import * as loginActions from '../Action/action.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router'

class LoginForm extends React.Component {
    constructor(props){
        super(props);
        this.state={
            mailid:"",
            password:"",
         }
         this.updateState = this.updateState.bind(this); 
         this.login = this.login.bind(this);
        };
           
       updateState(e) {
            this.setState(
                 {[e.target.name]: e.target.value
             })
        }
        login() {
            var data = {
                emailId : this.state.mailid,
                password : this.state.password 
            }
            this.props.action.login(data);
        }
     
    render(){

        return(
            <div className="login-container">
            {/*<form Submit={this.Submit}>*/}
            <label>email</label>  
            <input type="text" name="mailid" 
                onChange={this.updateState}
                />
             <label>password</label>
             <input type="password" name="password"  
              onChange={this.updateState}
                />
            <div className ="form-group"><button className="btn btn-primary-lg" onClick={this.login} >Login</button>
            </div>

            {/*</form>*/}
            </div>
            
        );
        }
    }

function mapStateToProps(state) {
    return {
        errorMessage: state.LoginReducer.loginError
    };
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(loginActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
    
