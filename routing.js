import React from 'react';
import LoginContainer from './modules/Login/Container/LoginContainer';
//import {Switch} from 'react-router';
import { Router, Route, browserHistory, history, IndexRoute } from 'react-router'
import NotFound from './modules/notFound/Container/notFound';
import DashboardContainer from './modules/Dashboard/Container/DashboardConatiner';

class Routing extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        
        return (
            <Router history={browserHistory} location={history}>
                <Route>
                    <IndexRoute component={LoginContainer} />
                    <Route exact path = "/" component={LoginContainer} />
                    <Route exact path = "/dashboard" component={DashboardContainer} />
                    <Route path='*' component={NotFound} />
                    </Route>
            </Router>
        )
    }
}
export default Routing;