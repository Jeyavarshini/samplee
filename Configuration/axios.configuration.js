import axios from 'axios';
import {API_URL} from './ApiBase';

export const HTTP_AUTH_URL = axios.create({
  baseURL: API_URL
});

export const HTTP_SERVER_URL = axios.create({
baseURL: API_URL,
headers: {
 // "Authorization": getCookie('Authorization')
  }
})