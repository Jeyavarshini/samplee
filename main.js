import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import Routing from './routing.js';
import Create from './modules/Store/Create.js';

const Store = Create();
ReactDOM.render(<Provider store={Store}><Routing /></Provider> , document.getElementById('app'));